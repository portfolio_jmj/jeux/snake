/**@type {HtmlElement} */
let snake;
let snakeHead;
let contraintLeft;
let contraintRight;
let contraintTop;
let contraintBottom;
let snakeSize = 8*2;
let velocity = snakeSize;
let listitem = ['♥︎'];
let listItemOnBoard = [];
let listSnakePartOnboard = [];
let board;
let mainMap = Array.from(Array(16), () => new Array(16));
let previousDirection = '';
let spawnItem;
let timeOutMovePartSnake;
let autoTimeOutMove;
let score = 0;
let scoreElement;

showBestScore();

function main(){
    init();
    spawnItem = setInterval(() => {
        addItem();
    }, 4000);
    addItem();
    setAutoMove('ArrowRight');
}

/**
 * Initialise le jeu
 */
function init(){
    showBestScore();
    document.body.addEventListener('keydown',onKeyPress)
    board = document.getElementsByTagName('main')[0];
    const position = board.getBoundingClientRect();

    contraintLeft = position.left;
    contraintRight = position.right - contraintLeft;
    contraintTop = position.top;
    contraintBottom = position.bottom  - contraintTop;

    const x = board.clientWidth / 2;
    const y = board.clientHeight / 2;

    snake = document.getElementById('snake');
    const snakePart = document.createElement('div');
    snakePart.id = "snakeHead";
    snakePart.style.height = snakeSize + 'px';
    snakePart.style.width = snakeSize + 'px';
    snakePart.textContent = ":";
    snakePart.style.top = y + 'px';
    snakePart.style.left = x + 'px';
    snakePart.className = 'snake-head'
    snakeHead = snakePart;
    snake.appendChild(snakePart);

    scoreElement = document.getElementById("current");
    score = 0;
    scoreElement.innerText = `Score : ${score}`;
}

/**
 * Fin de partie
 */
function endGame(){
    clearInterval(spawnItem);
    clearTimeout(timeOutMovePartSnake);
    clearInterval(autoTimeOutMove);
    clearInterval(autoTimeOutMove);
    saveScore();
    listItemOnBoard = [];
    listSnakePartOnboard = [];
    previousDirection = '';
    document.body.removeEventListener('keydown',onKeyPress)
    snake.innerHTML = '';
    snakeSize = 8*2;
    velocity = snakeSize;
    board.innerHTML = '';
    board.appendChild(snake);
}

/**
 * Déplacement de la tête du snake
 * @param {*} event 
 */
function onKeyPress(event){
    if(event.preventDefault)
        event.preventDefault();
    const key = event.key;
    console.log('key',key)
    const x = parseInt(snakeHead.style.left.split("px")[0]);
    const y = parseInt(snakeHead.style.top.split("px")[0]);
    const lastPosition = snakeHead.getBoundingClientRect();
    if(limitMove(key))
        return;
    
    clearInterval(autoTimeOutMove);
    setAutoMove(key);
    previousDirection = key;

    let newPosition;
    switch (key) {
        case 'ArrowLeft':
            newPosition = (x - velocity);
            if(newPosition < 0)
            snakeHead.style.left = 304 + "px";
            else
            snakeHead.style.left = (x - velocity) + "px";

            snakeHead.style.transform = 'rotateZ(180deg)';
            break;
        case 'ArrowRight':
            newPosition = (lastPosition.x + velocity);
            if((contraintRight) <= newPosition)
                snakeHead.style.left = 0 + "px";
            else
            snakeHead.style.left = (x + velocity) + "px";

            snakeHead.style.transform = 'rotateZ(0deg)';
        break;
        case 'ArrowUp':
            newPosition = (y - velocity);
            if(newPosition < 0)
            snakeHead.style.top = 304 + "px";
            else
            snakeHead.style.top = (y - velocity) + "px";

            snakeHead.style.transform = 'rotateZ(-90deg)';
        break;
        case 'ArrowDown':
            newPosition = (lastPosition.y + velocity);
            if((contraintBottom) <= newPosition)
            snakeHead.style.top = 0 + "px";
            else
            snakeHead.style.top = (y + velocity) + "px";

            snakeHead.style.transform = 'rotateZ(90deg)';
        break;
        default:
            break;
    }

    checkCollision();
    moveSnakeBody(1,{'x': x, 'y': y},key)

}

/**
 * créer et ajoute une partie du snake
 */
function createSnakePart(){
    const x = parseInt(snake.lastElementChild.style.left.split("px")[0]) - snakeSize;
    const y = parseInt(snake.lastElementChild.style.top.split("px")[0]);
    const snakePart = document.createElement('div');
    snakePart.style.height = snakeSize + 'px';
    snakePart.style.width = snakeSize + 'px';
    snakePart.style.backgroundColor = 'black';
    snakePart.style.position = 'absolute';
    snakePart.style.top = y + 'px';
    snakePart.style.left = x + 'px';
    snakePart.style.boxSizing = 'border-box';
    snakePart.style.border = '1px solid red'
    listSnakePartOnboard.push(snakePart);
    snake.appendChild(snakePart);
}

/**
 * Déplace les éléments du snake
 * @param {*} index 
 * @param {*} plastPosition 
 * @param {*} key 
 * @returns 
 */
function moveSnakeBody(index,plastPosition,key){
    try {
        const c = snake.childElementCount;
        if(index >= c) return;
        timeOutMovePartSnake = setTimeout(() => {
            const element = snake.children[index];
            let x = parseInt(element.style.left.split("px")[0]);
            let y = parseInt(element.style.top.split("px")[0]);
    
            const vlastPosition = {'x':x,'y':y};
        
            x = plastPosition.x;
            y = plastPosition.y;
    
            element.style.left = x + 'px';
            element.style.top = y + 'px';
    
            moveSnakeBody(index+1,vlastPosition,key)
        }, 500);
    } catch (error) {
        endGame();
    }
}

/**
 * Ajoute un item dans une position random
 */
function addItem(){
    const rndRow = Math.floor(Math.random() * 15);
    const rndCol = Math.floor(Math.random() * 15)

    const x = rndRow * 16;
    const y = rndCol * 16;

    const item = document.createElement('div');
    item.style.height = snakeSize + 'px';
    item.style.width = snakeSize + 'px';
    item.style.position = 'absolute';
    item.style.top = y + 'px';
    item.style.left = x + 'px';
    item.style.textAlign = 'center';
    item.innerHTML = listitem[0];
    listItemOnBoard.push(item);
    board.appendChild(item);
}

function checkCollision(){
    const headX = parseInt(snakeHead.style.left.split("px")[0]);
    const headY = parseInt(snakeHead.style.top.split("px")[0]);
    const positionHead = {x:headX,y:headY};
    const index = listItemOnBoard.findIndex((element)=> {
        const vPositionElement = extractPosition(element);
        return vPositionElement.x == positionHead.x && vPositionElement.y == positionHead.y
    });
    if(index > -1){
        listItemOnBoard[index].remove();

        listItemOnBoard = listItemOnBoard.filter((el) => {
            const vPositionElement = extractPosition(el);
            return !(vPositionElement.x == positionHead.x && vPositionElement.y == positionHead.y) 
            });
        createSnakePart();
        scoreAdd();
        return;
    }


    const indexSnakePart = listSnakePartOnboard.findIndex((element)=> {
        const vPositionElement = extractPosition(element);
        return vPositionElement.x == positionHead.x && vPositionElement.y == positionHead.y
    });
    if(indexSnakePart > -1){
        alert('fin de la game');
        endGame();
    }
}

/**
 * Extrait la position d'un elementhtml
 * @param {HtmlElement} htmlItem 
 * @returns {Position}
 */
function extractPosition(htmlItem){
    const vPosition = new Position();
    vPosition.x = parseInt(htmlItem.style.left.split("px")[0]);
    vPosition.y = parseInt(htmlItem.style.top.split("px")[0]);
    return vPosition;
}

/**
 * 
 * @param {string} key 
 * @returns {boolean}
 */
function limitMove(key){
    if(snake.childElementCount == 1)
        return false;

    let vReturn = false;
    switch (key) {
        case 'ArrowLeft':
            if(previousDirection == 'ArrowRight')
                vReturn = true;
        break;
        case 'ArrowRight':
            if(previousDirection == 'ArrowLeft')
            vReturn = true;
        break;
        case 'ArrowTop':
            if(previousDirection == 'ArrowBottom')
            vReturn = true;
        break;
        case 'ArrowBottom':
            if(previousDirection == 'ArrowTop')
            vReturn = true;
        break;   
        default:
            break;
    }

    return vReturn;
}

/**
 * doit faire avancer le snake tout seul
 * dans la dernière direction choisi par l'utilisateur
 */
function setAutoMove(pKey){
    autoTimeOutMove = setInterval(() => {
        const ev = new Event('keydown');
        ev.key = pKey;
        onKeyPress(ev);
    }, 150);
    
}

/**
 * Incrémentation du score
 */
function scoreAdd(){
    score += 5;
    scoreElement.innerText = `Score : ${score}`;
}

/**
 * persistance du meilleur score
 */
function saveScore(){
    let bestScore = localStorage.getItem('snakeScore');
    if (bestScore < score){
        localStorage.setItem('snakeScore',score)
    }
}

/**
 * Affichage du meilleur score
 */
function showBestScore(){
    let bestScore = localStorage.getItem('snakeScore');
    if (bestScore != null){
        document.getElementById("best").innerText = `Best Score : ${bestScore}`;
    }
}


class Position{
    /**@type {Number} */
    x;
    /**@type {Number} */
    y;
}